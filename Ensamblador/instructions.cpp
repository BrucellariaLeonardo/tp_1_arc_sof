#include "instructions.h"

namespace inst
{
    //METODOS PRIVADOS
    int Instruction ::load(int n)
    { //Retorna el numero de instruccion de load sumado al operando
        return (0x01000000 + n);
    }

    int Instruction ::add(int n)
    { // Retorana el numero de instruccion de add sumado al operando
        int ret;
        if (n >= 0)
        {
            ret = (0x02000000 + n); 
        }else{
            ret = (0x02000000 + 0x01000000 + n );   // opcode + ofset + numero negativo
        }
        return ret;
    }

    int Instruction ::print()
    { // Retorana el numero de instruccion de print
        return (0x03000000);
    }

    //CONSTRUCTORES
    Instruction ::Instruction()
    {
    }

    Instruction ::Instruction(string opcode)
    {
        this->opcode = opcode;
    }
    Instruction ::Instruction(string opcode, string op)
    {
        this->opcode = opcode;
        this->op = op;
    }

    //METODOS
    int Instruction ::toBin()
    { //recibe una linea de codigo y la devuelve traducida a codigo maquina
        stringstream ss;
        int aux;
        int ret = -1; // se retornara -1 en caso de un op code invalido
        if (this->opcode == "LOAD")
        {                   //si se tiene una instruccion tipo load
            ss << this->op; // se transforma el operador de string a int
            ss >> aux;
            ret = load(aux); // y se lo envida a load
        }
        else if (this->opcode == "ADD")
        { //de forma analoga con add
            ss << this->op;
            ss >> aux;
            ret = add(aux);
        }
        else if (this->opcode == "PRINT")
        { // en caso de print, como print no tiene operando, directamente se llama a print
            ret = print();
        }
        else if (this->opcode == "") //en caso de una linea vacia, se devuelve 0
        {
            ret = 0;
        }
        return ret; // si llegue a esta linea quiere decir que el op code es incorrecto
    }
}