#ifndef FILEMANAGE_H
#define FILEMANAGE_H

#include "instructions.h"
#include <fstream>

using namespace std;
using namespace inst;

namespace flmg
{
    class Fmanag
    {
    private:
        ifstream file; // putero al archivo de  texto
        ofstream binf; // puntero al archivo binario
    public:
        
        Fmanag(string inf, string outf);    // setea los punteros a los archivos de lectura y escritura.
        bool endfile();                     // retorna true si se esta en el final del archivo, false caso contrario
        void writeInst(int out);            //se escribe una linea de codigo maquina en un archivo .bin de salida
        Instruction getInst();              //toma una linea del codigo assembler y retorna un objeto instruccion que corresponde a la linea
        void fclose();                      // Cierra los archivos utilizados por el compilador

    }; //Lee lineas de codigo de un archivo de texto y devuelve instrucciones

}// namespace de file manager

#endif