//Librerias
#include "instructions.h"
#include "filemanage.h"

using namespace std;
using namespace inst;       // name space instructions.h
using namespace flmg;       // name space filemanage.h


// Se recibe primero el archivo de entrada .txt y luego el archivo binario .bin    
int main(int n, char* argv [])
{

    Fmanag manag(argv[1],argv[2]);               //se genera el administrador de archivo
    Instruction inst;                           
    int instCont = 0;                           // lleva cuenta de que linea se esta leyendo
    int binaryInst;
    while(!manag.endfile()){                        // mientras no se haya llegado al final del archivo
        inst =manag.getInst();              // tomo instrucciones del archivo de salida
        instCont++;
        binaryInst = inst.toBin();           // se almacena la instruccion en binario o su codigo de error
        switch (binaryInst)
        {
        case 0:                 // en caso de 0 se paso por una linea vacia, se ignora la linea y se pasa a la siguiente inst
            break;
        case -1:                // en caso de -1 se ingreso un op_code invalido, se retorna un mensaje diciendo la linea defectuosa
            printf("Error de sintaxis en la linea %d", instCont);
            manag.fclose();     // cierro el archivo antes de terminar la ejecucion.
            exit(-4);
            break; 
        default:
            manag.writeInst(binaryInst);       //si no hay error escribo en el archivo de salidas
            break;
        }
    }
    manag.fclose();
    return 0;
}