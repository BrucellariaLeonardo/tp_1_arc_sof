#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <iostream>
#include <sstream>

using namespace std;

namespace inst
{

    class Instruction
    {
    private:
        string opcode;
        string op;
        int load(int n);

        int add(int n);

        int print();

    public:
        //CONSTRUCTORES
        Instruction();
        Instruction(string opcode);
        Instruction(string opcode, string op);

        //METODOS
        int toBin(); //recibe una linea de codigo y la devuelve traducida a codigo maquina

    }; // instrucciones que se traducen a codigo maquina
}

#endif