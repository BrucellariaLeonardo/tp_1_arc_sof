#include "filemanage.h"

namespace flmg
{

    Fmanag :: Fmanag(string inf, string outf){                         // setea los punteros a los archivos de lectura y escritura
        file.open(inf, ios::in);
        binf.open(outf, ios:: out | ios::binary | ios::trunc);
        if(file.fail()){                                    // si no se pude abrir el codigo assembler, retorno error -1
            cout << "Fallo al abrir el archio" << endl;
            exit(-1);
        }
        if(binf.fail()){                                    // si no se pudo generar archivo binario, retorno error  -2
            cout << "Fallo generando archivo de salida"<< endl;
            exit(-2);
        }
        return;
    }
    
    bool Fmanag :: endfile (){               // retorna true si se esta en el final del archivo, false caso contrario
        bool r=this->file.eof();               
        return r ;
    }

    void Fmanag :: writeInst(int out)  //se escribe una linea de codigo maquina en un archivo .bin de salida
    {
        binf.write((const char*) &out, sizeof(out));
        return;
    }
    
    Instruction Fmanag :: getInst(){                  //toma una linea del codigo assembler y retorna un objeto instruccion que corresponde a la linea
        string buffer[2];
        Instruction ret;
        if(file.eof()){                     // Se corrobora el no estar al final de archivo
            cout << "Fallo al tomar instruccion" << endl;
            exit(-3);
        }

        getline(file,buffer[0]);      // tomo la siguiente instruccion
        if(buffer[0] == "PRINT")            // me fijo si es print, ya que print no posee operando
        {
            ret = Instruction (buffer[0]);     // si es print genero el objeto instruccion que le corresponde
        }else                               // sino discrimino el operando del op code
        {
            istringstream ss (buffer[0]);
            getline(ss, buffer[0], ' ');
            getline(ss, buffer[1]);
            ret = Instruction (buffer[0],buffer[1]);
        }
        return ret;
    }                     // se retorna la instruccion

    void Fmanag :: fclose()       // Cierra los archivos utilizados por el compilador
    {
        file.close();
        binf.close();
        return;
    }

}// name space file mange