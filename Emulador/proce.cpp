#include "proce.h"
#include <iostream>
#include <iomanip>
#include <string>
namespace pros
{
    Processor ::Processor() // se construye el procesador con el acumulador seteado en 0
    {
        this->acum = 0;
        this->pc = 1;
        this->debug=false;
        return;
    }
    void Processor ::load(unsigned int operand)
    {
        if(this->debug)
            std::cout << "LOAD " << operand << std::endl;
        this->acum = operand; //setea el acumulador en el valor del operando
        // TEST std::cout << std::setbase(16) << this->acum << std::endl;
        return;
    }
    void Processor ::print()
    {
        if(this->debug)
            std::cout << "PRINT" << std::endl;
        if (acum <= 0xF00000) // si el numero es positivo
        {
            std ::cout <<  this->acum << std ::endl; //imprime el acumulaor
        }
        else // el numero es negativo
        {
            std ::cout <<"-"<< (int)(0xFFFFFE - this->acum) << std ::endl; // se pone un ofset en el print y se muestra por patalla
        }

        return;
    }
    void Processor ::add(unsigned int operand)
    {
        if(this->debug){
            if(operand<=0xF00000)
                std::cout << "ADD " << operand << std::endl;
            else
                std::cout << "ADD " <<"-"<< (int)(0xffffff - (operand ) + 1) << std::endl;
        }
        if (operand <= 0xF00000) // si el numero es positivo
        {
            this->acum += operand; //suma el operando al acumulador
        }
        else
        {                          //sino el numero es negativo
            this->acum -= (0xffffff - (operand ) + 1); //resta el operando al acumulador
        }
        //TO DO: CASO DE DESBORDE ACUMULADOR, DECIDIR SI INTERPRETARLO O SEGUIR EL PROGRAMA
    }
    void Processor ::runInst(unsigned int inst)
    {
        unsigned int codigo, operador;
        codigo=inst >> 24;
        operador=(inst & 0xFFFFFF);

        switch (codigo)
        {
        case 1:
                // TEST std::cout << "ingresando a load" << std::endl;
            load(operador);
            break;
        case 2:
                // TEST std::cout << "ingresando a add" << std::endl;
            add(operador);
            break;
        case 3:
                // TEST std::cout << "ingresando a print" << std::endl;
            print();
            break;
        default:
            break;
        }
    pc++;   //actualizo el contador de programa
    return ;
    }

    void Processor ::debuger(std::string debuggin){
        if(debuggin=="debug")
            this->debug=true;
        return;
    }

}