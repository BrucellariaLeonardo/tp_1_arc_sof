#include<iostream> 
#include "fileManager.h"
#include "programMemory.h"
#include "proce.h"
#include <iomanip>
using namespace std;
using namespace fm;
using namespace pm;
using namespace pros;

//Para activar el modo debug escriba debug como segundo argumento en la ejecucion en la consola
//Este programa recibe como primer argumento en consola el nombre del archivo .bin

int main(int argc ,char* argv[])
{
    FileManager file = FileManager(argv[1]);                   
    ProgramMemory memory = ProgramMemory(file.getInst());
    Processor proce = Processor();
    if(argc>2)
        proce.debuger(argv[2]);
    while (!file.endf())                                    //Cargo las instrucciones en memoria
    {
        memory.genNew(file.getInst());
    }
    file.close();                                           //cierro el archivo binario

    while(!memory.progEnd())                                //mientras no se haya terminado el programa
    {
        proce.runInst(memory.getInst());                    //cargo instrucciones de la memoria y las ejectuo en el procesador
    }
    memory.freeMem();                                       //libero la memoria usada durante el programa
    return 0;
}