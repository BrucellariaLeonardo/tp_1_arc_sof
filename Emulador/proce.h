#ifndef PROCE_H
#define PROCE_H
#include <fstream>
#include <string>
namespace pros
{
    class Processor //recibe el archivo de programa y se encargar de leer sus instrucciones
    {
    private:
        unsigned int acum;
        int pc;
        bool debug;
        void load(unsigned int operand);    //carga x al acumulador
        void print(void);                   // imprime el acumulador
        void add(unsigned int operand);     // suma x al acumulador
    public:
        Processor();     
        void runInst(unsigned int inst);
        void debuger(std::string debuggin);
        //void getInst(unsigned int inst);  // Recibe una inst separa su op code de su operando y luego corre la instruccion
    };
}
#endif