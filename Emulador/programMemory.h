#ifndef PROGRAMMEMORY_H
#define PROGRAMMEMORY_H

namespace pm
{
    typedef struct inst // instrucciones que seran almacenadas en la memoria de programa
    {
        unsigned int inst; //instruccion
        struct inst *next; // puntero a la siguiente instruccion
    } Instruction;

    class ProgramMemory
    {
    private:
        Instruction *first, *siguiente, *last;
        unsigned int inst;

    public:
        ProgramMemory(unsigned int firstInst); //constructor, cuando recibo los nibles.
        void freeMem();
        void genNew(unsigned int newInst); // aloca un nodo al final de la lista y lo setea
        unsigned int getInst();            //recibe dos punteros donde se almacenaran el opcode y el operando
        bool progEnd();                    //retorna true si no hay instruccion siguiente.
    };
}

#endif