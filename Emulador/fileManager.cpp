#include "fileManager.h"
#include <iostream>
#include <iomanip>
namespace fm
{
    FileManager::FileManager(std::string nombre_archivo) // El constructor abre el archivo .bin
    {
        inf.open(nombre_archivo, std::ios::in);
        if (inf.fail())
        { // si no se pude abrir el codigo assembler, retorno error -1
            std::cout << "Fallo al abrir el archio" << std::endl;
            exit(-1);
        }
    }

    unsigned int FileManager ::getInst()
    {
        unsigned int newInst;
        this->inf.read((char *) &newInst, sizeof(newInst));
        /* DEBUG
        std::cout << std::setw(8) <<std::setfill('0') <<std::setbase(16) << newInst << std::endl;
        */
        return newInst;
    }
    bool FileManager::endf()
    {
        return this->inf.eof();
    }

    void FileManager::close()
    {
        this->inf.close();
        return;
    }
}