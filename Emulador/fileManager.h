#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include <fstream>
namespace fm
{
    class FileManager //recibe el archivo de programa y se encargar de leer sus instrucciones
    {
    private:
        std::ifstream inf;

    public:
        FileManager(std::string file); // Setea inf para que apunte al archivo de entrada
        unsigned int getInst();        // Retorna una instruccion leida del binario
        bool endf();                   // retorna true si se llego al final del archivo
        void close();                  // cierra el binario.
    };
}
#endif