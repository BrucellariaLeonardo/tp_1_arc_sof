#include "programMemory.h"
#include <iostream>
namespace pm
{

    ProgramMemory ::ProgramMemory(unsigned int firstInst) //constructor, genera los punteros las inst de programa y carga la primer instruccion
    {
        this->first = new Instruction; //aloca y acomoda punteros
        this->siguiente = this->first;
        this->last = this->first;

        this->first->inst = firstInst; //setea la primer instruccion
    }

    void ProgramMemory::genNew(unsigned int newInst) //aloca un nuevo nodo al final y lo setea
    {
        this->last->next = new Instruction; //aloca el nodo
        this->last = this->last->next;      //actualizo el puntero al final
        this->last->inst = newInst;         //seteo la inst
        this->last->next = NULL;
        return;
    }

    unsigned int ProgramMemory::getInst()
    {
        unsigned int inst;
        if (this->siguiente != NULL)
        {
            inst = this->siguiente->inst;
            this->siguiente = this->siguiente->next; //next es el nodo al que apunta siguiente.
        }
        else
        {
            inst = 0; //en caso de que no haya siguiente operacion retorno una instruccion que denota error
        }
        return inst;
    }

    bool ProgramMemory::progEnd()
    {
        bool ret;
        if (this->siguiente->next->next == NULL) //si no hay siguiente instruccion
        {
            ret == true; //fin del programa
        }
        else
        {
            ret = false; //sino el programa sigue
        }
        return ret;
    }

    void ProgramMemory :: freeMem()     //libero los nodos de memoria
    {
        Instruction *aux;               // puntero usado para liberar la memoria
        while(first != NULL)            // mientras el primero siga conteniendo instrucciones libero la memoria
        {
            aux = first;
            first = first->next;
            delete aux;
        }
        return;
    }
}
